package main

import (
	"flag"
	"fmt"
	"time"
)

type Params struct {
	BaseURL         string
	APIKey          string
	APISecret       string
	DomainName      string
	RecordName      string
	UpdateFrequency time.Duration
}

func getCLIFlags(args []string) (*Params, error) {
	flagSet := flag.NewFlagSet(
		"GoDaddyUpdater",
		flag.ExitOnError,
	)
	var durationExpression string
	params := Params{}
	flagSet.StringVar(&params.BaseURL, "url", "https://api.godaddy.com", "GoDaddy API Base URL (https://api.godaddy.com or https://api.ote-godaddy.com/), default to https://api.godaddy.com")
	flagSet.StringVar(&params.APIKey, "key", "", "GoDaddy API Key [required]")
	flagSet.StringVar(&params.APISecret, "secret", "", "GoDaddy API Secret [required]")
	flagSet.StringVar(&params.DomainName, "domain", "", "GoDaddy domain to update [required]")
	flagSet.StringVar(&params.RecordName, "record", "@", "GoDaddy record to update, default to '@'")
	flagSet.StringVar(&durationExpression, "freq", "15s", "IP Address change check frequency, like 300ms, 1.5h or 2h45m. Valid time units are ns, us (or µs), ms, s, m, h. Defaults to 15s (15 seconds)")
	err := flagSet.Parse(args)
	if err != nil {
		return nil, err
	}
	if params.APIKey == "" {
		return nil, fmt.Errorf("GoDaddy API Key [-key] is required")
	}
	if params.APISecret == "" {
		return nil, fmt.Errorf("GoDaddy API Secret [-secret] is required")
	}
	if params.DomainName == "" {
		return nil, fmt.Errorf("GoDaddy Domain Name to update [-domain] is required")
	}
	params.UpdateFrequency, err = time.ParseDuration(durationExpression)
	if err != nil {
		return nil, fmt.Errorf("error parsing duration expression %#v, %s", durationExpression, err.Error())
	}
	return &params, nil
}
