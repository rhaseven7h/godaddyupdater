package godaddyapi

type GoDaddyAPI struct {
	APIBaseURL string
	APIKey     string
	APISecret  string
}

type GoDaddyDNSRecord struct {
	Data string `json:"data"`
	Name string `json:"name"`
	TTL  uint64 `json:"ttl"`
	Type string `json:"type"`
}

type GoDaddyDNSRecords []GoDaddyDNSRecord

type GoDaddyErrorField struct {
	Code        string `json:"code"`
	Message     string `json:"message"`
	Path        string `json:"path"`
	PathRelated string `json:"pathRelated"`
}

type GoDaddyError struct {
	Code    string `json:"code"`
	Message string `json:"message"`
	Fields  []GoDaddyErrorField
}

/*

	[
	  {
	    "data": "string",
	    "name": "string",
	    "port": 0,
	    "priority": 0,
	    "protocol": "string",
	    "service": "string",
	    "ttl": 0,
	    "weight": 0
	  }
	]

*/

type GoDaddyDNSRecordForUpdate struct {
	Data     string `json:"data,omitempty"`
	Name     string `json:"name,omitempty"`
	Port     uint64 `json:"port,omitempty"`
	Priority uint64 `json:"priority,omitempty"`
	Protocol string `json:"protocol,omitempty"`
	Service  string `json:"service,omitempty"`
	TTL      uint64 `json:"ttl,omitempty"`
	Weight   uint64 `json:"weight,omitempty"`
}

type GoDaddyDNSRecordsForUpdate []GoDaddyDNSRecordForUpdate
