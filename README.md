# GoDaddy DNS A Record Updater

**Command**: godaddyupdater

GoDaddy DNS A Record Updater

## Algorithm

2. Set cached A record to nil
4. Every N seconds
    1. Get current IP address
    2. If cached A record is invalid or if cached  record is different
        1. Update A record
        2. Cache A record
    4. Sleep for a while

## Suggested SupervisordD configuration

```toml
[program:godaddyupdater]
command=/root/go/bin/godaddyupdater -domain yourdomain.tld -key your_godaddy_api_key -secret your_godaddy_api_secret
autostart=true
autorestart=true
startretries=10
stdout_logfile=/var/log/godaddyupdater/godaddyupdater.log
stdout_logfile_maxbytes=50MB
stdout_logfile_backups=10
redirect_stderr=true
```

