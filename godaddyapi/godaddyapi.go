package godaddyapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/sirupsen/logrus"
)

func NewGoDaddyAPI(APIBaseURL string, APIKey string, APISecret string) *GoDaddyAPI {
	return &GoDaddyAPI{
		APIBaseURL: APIBaseURL,
		APIKey:     APIKey,
		APISecret:  APISecret,
	}
}

func (gd *GoDaddyAPI) GetCurrentRecordsForType(domain string, recordType string) (GoDaddyDNSRecords, error) {
	return gd.GetCurrentRecordsForTypeAndName(domain, recordType, "")
}

func (gd *GoDaddyAPI) GetCurrentRecordsForTypeAndName(
	domain string,
	recordType string,
	recordName string,
) (GoDaddyDNSRecords, error) {
	goDaddyDNSRecords := make([]GoDaddyDNSRecord, 0, 1)
	goDaddyURLString := fmt.Sprintf("%s/v1/domains/%s/records/%s", gd.APIBaseURL, domain, recordType)
	if recordName != "" {
		goDaddyURLString = fmt.Sprintf("%s/%s", goDaddyURLString, recordName)
	}
	req := &http.Request{}
	req.Method = http.MethodGet
	goDaddyURL, err := url.Parse(goDaddyURLString)
	if err != nil {
		return nil, err
	}
	req.URL = goDaddyURL
	authenticationHeader := fmt.Sprintf("sso-key %s:%s", gd.APIKey, gd.APISecret)
	req.Header = http.Header{}
	req.Header.Add("Authorization", authenticationHeader)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	jsonBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(jsonBytes, &goDaddyDNSRecords)
	if err != nil {
		return nil, err
	}
	return goDaddyDNSRecords, nil
}

func (gd *GoDaddyAPI) SetCurrentRecordsForType(
	domain string,
	data interface{},
	recordType string,
) (*GoDaddyError, error) {
	return gd.SetCurrentRecordsForTypeAndName(domain, data, recordType, "")
}

func (gd *GoDaddyAPI) SetCurrentRecordsForTypeAndName(
	domain string,
	data interface{},
	recordType string,
	recordName string,
) (*GoDaddyError, error) {
	var jsonBytes []byte
	goDaddyURLString := fmt.Sprintf("%s/v1/domains/%s/records/%s", gd.APIBaseURL, domain, recordType)
	if recordName != "" {
		goDaddyURLString = fmt.Sprintf("%s/%s", goDaddyURLString, recordName)
	}
	logrus.
		WithField("url", goDaddyURLString).
		Info("GoDaddy API call URL")

	jsonBytes, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	logrus.WithField("body", string(jsonBytes)).Info("GoDaddy API call Payload")

	authenticationHeader := fmt.Sprintf("sso-key %s:%s", gd.APIKey, gd.APISecret)

	req, err := http.NewRequest(http.MethodPut, goDaddyURLString, strings.NewReader(string(jsonBytes)))
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", authenticationHeader)
	req.Header.Add("accept", "application/json")
	req.Header.Add("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == http.StatusOK {
		return nil, nil
	}

	jsonBytes, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	logrus.
		WithField("body", string(jsonBytes)).
		Info("SetCurrentRecordsForTypeAndName GoDaddy call response payload")

	goDaddyError := &GoDaddyError{}
	err = json.Unmarshal(jsonBytes, goDaddyError)
	if err != nil {
		return nil, err
	}
	return goDaddyError, nil
}
