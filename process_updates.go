package main

import (
	"database/sql"
	"fmt"
	"os"
	"time"

	"github.com/sirupsen/logrus"

	"bitbucket.org/rhaseven7h/godaddyupdater/godaddyapi"
	"bitbucket.org/rhaseven7h/godaddyupdater/myipapi"
)

func updateGoDaddyDNSARecord(
	goDaddyAPI *godaddyapi.GoDaddyAPI,
	domainName string,
	recordName string,
	ipAddress string,
	cachedAddress *sql.NullString,
) {
	goDaddyError, err := goDaddyAPI.SetCurrentRecordsForTypeAndName(
		domainName,
		godaddyapi.GoDaddyDNSRecordsForUpdate{
			godaddyapi.GoDaddyDNSRecordForUpdate{
				Data: ipAddress,
			},
		},
		"A",
		recordName,
	)
	if err != nil {
		logrus.WithError(err).Error("http client error updating GoDaddy DNS A record")
		return
	}
	if goDaddyError != nil {
		logrus.WithError(
			fmt.Errorf("error [%s]: %s", goDaddyError.Code, goDaddyError.Message),
		).Error("server returned error updating GoDaddy DNS A record")
		return
	}
	cachedAddress.String = ipAddress
	cachedAddress.Valid = true
	logrus.Infof("GoDaddy DNS A record updated with [%s]", ipAddress)
}

func processUpdates(
	domainName string,
	recordName string,
	goDaddyAPI *godaddyapi.GoDaddyAPI,
	ticker <-chan time.Time,
	showStopper chan os.Signal,
	endSignal chan<- bool,
) func() {
	cachedAddress := &sql.NullString{Valid: false, String: ""}
	return func() {
		for {
			select {
			case <-ticker:
				logrus.Info("initiating GoDaddy DNS A record update")
				currentIPAddressInfo, err := myipapi.GetCurrentExternallyVisibleIPAddress()
				if err != nil {
					logrus.WithError(err).Error("error getting current IP address, aborting update")
					continue
				}
				currentIPAddress := currentIPAddressInfo.IP
				if !cachedAddress.Valid || (cachedAddress.Valid && cachedAddress.String != currentIPAddress) {
					logrus.Infof(
						"update needed, current IP address is %s, cached is NullString{valid: %t, string: '%s'}",
						currentIPAddress,
						cachedAddress.Valid,
						cachedAddress.String,
					)
					updateGoDaddyDNSARecord(
						goDaddyAPI,
						domainName,
						recordName,
						currentIPAddress,
						cachedAddress,
					)
				} else {
					logrus.Info("no updated needed")
				}
				logrus.Info("update sequence finished")
			case <-showStopper:
				logrus.Info("terminate signal received")
				endSignal <- true
				return
			}
		}
	}
}
