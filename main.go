package main

import (
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"

	"bitbucket.org/rhaseven7h/godaddyupdater/godaddyapi"
)

func main() {
	logrus.SetFormatter(&logrus.TextFormatter{})
	logrus.Info("starting Go Daddy DNS A Record Updater ...")

	cliFlags, err := getCLIFlags(os.Args[1:])
	if err != nil {
		logrus.WithError(err).Error("argument error")
		return
	}

	goDaddyAPI := godaddyapi.NewGoDaddyAPI(
		cliFlags.BaseURL,
		cliFlags.APIKey,
		cliFlags.APISecret,
	)

	domainName := cliFlags.DomainName

	recordName := cliFlags.RecordName

	ticker := time.Tick(cliFlags.UpdateFrequency)

	showStopper := make(chan os.Signal)

	endSignal := make(chan bool)

	signal.Notify(
		showStopper,
		syscall.SIGHUP,
		syscall.SIGQUIT,
		syscall.SIGTERM,
		syscall.SIGINT,
	)

	go processUpdates(
		domainName,
		recordName,
		goDaddyAPI,
		ticker,
		showStopper,
		endSignal,
	)()

	<-endSignal
	logrus.Info("terminating ...")
	time.Sleep(500 * time.Millisecond)
	logrus.Info("done.")
}
