package myipapi

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

const (
	MyIPServiceURL = "https://api.myip.com"
)

type IPAddressInfo struct {
	IP          string `json:"ip"`
	Country     string `json:"country"`
	CountryCode string `json:"cc"`
}

func GetCurrentExternallyVisibleIPAddress() (*IPAddressInfo, error) {
	ipAddressInfo := &IPAddressInfo{}
	resp, err := http.Get(MyIPServiceURL)
	if err != nil {
		return nil, err
	}
	jsonResponseBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(jsonResponseBytes, ipAddressInfo)
	if err != nil {
		return nil, err
	}
	return ipAddressInfo, nil
}
